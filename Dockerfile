# Customize the ace image with some scripts
FROM shadowx4fox/ace-toolkit:11.0.0.9-r1-amd64 as ace-builder
COPY docker/scripts/ace/*.sh /usr/local/bin/
USER root
RUN yum install -y gtk2 libXtst xorg-x11-server-Xvfb
RUN mkdir -p /home/aceuser/workspace
RUN chown -R aceuser:aceuser /home/aceuser/workspace
RUN chmod 755 /usr/local/bin/* 
USER aceuser


# Compile the service
FROM ace-builder as service-builder
COPY docker/sample-service/bars_override /home/aceuser/bar_overrides
COPY src/sample-service /home/aceuser/workspace
RUN ace_create_bar.sh -data /home/aceuser/workspace -p CustomerDatabaseJava -compileOnly
RUN ace_package_bar.sh -a /home/aceuser/bars/sample-service.bar -w /home/aceuser/workspace -k CustomerDatabaseV1 -o CustomerDatabaseJava/CustomerDatabaseJava.jar
RUN ace_override_bars.sh

# Build the service
FROM ibmcom/ace-mqclient:11.0.0.9-r1-amd64 as service
COPY --from=service-builder /home/aceuser/bars /home/aceuser/bars
USER root
RUN chown -R aceuser:aceuser /home/aceuser/bars
USER aceuser
RUN ace_compile_bars.sh
