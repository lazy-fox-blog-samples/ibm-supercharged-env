#!/bin/bash

if [ -z "$MQSI_VERSION" ]; then
  source /opt/ibm/ace-11/server/bin/mqsiprofile
fi

Xvfb :99 &
export DISPLAY=:99
mqsipackagebar "$@"