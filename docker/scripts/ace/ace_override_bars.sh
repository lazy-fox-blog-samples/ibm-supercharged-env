#!/bin/bash

if [ -z "$MQSI_VERSION" ]; then
  source /opt/ibm/ace-11/server/bin/mqsiprofile
fi

if ls /home/aceuser/bar_overrides/*.properties >/dev/null 2>&1; then
  for prop in /home/aceuser/bar_overrides/*.properties
  do
    propfile="${prop##*/}"
	bar="${propfile%%.*}"
	bar+=".bar"
	mqsiapplybaroverride -b "/home/aceuser/bars/$bar" -p $prop -r 
  done
fi